Dev environment setup instructions
Architecture: Asp.net Angular application

1 Download and install node js https://nodejs.org/en/ 
2 Download postgresql from https://www.enterprisedb.com/downloads/postgres-postgresql-downloads (install all options) 
3 Run through the setup and take note of your server credentials setup (go with username: postgres and password: ubu.1900) 
4 Once you have a server up and running go to pgAdmin and add a new server with the credentials setup 
5 Once a server is created create a blank database called uni db 
6 Now you can go to mporterApi\Powershell and run the powershell script. This will create the database tables and routines. Please note 
    make sure the postgresql version is correct according to the version on your local.
    Set-Location 'C:\Program Files\PostgreSQL\14\bin\'; in the powershell script is pointing to version 14 (latest stable). 
7 You can now run the app in visual studio and continue from the web page automatically opened up 
8 There is a test import csv on the repo.

Happy hunting!