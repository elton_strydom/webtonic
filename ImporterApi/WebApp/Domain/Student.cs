﻿namespace WebApp
{
  public class Student
  {
    public int id { get; set; }
    public string student_number { get; set; }
    public string name { get; set; }
    public string surname { get; set; }
  }
}
