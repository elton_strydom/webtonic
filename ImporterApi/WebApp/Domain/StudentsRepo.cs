﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Domain
{
    public class StudentsRepo
    {
        private readonly dataAdapter _dapper;
        public StudentsRepo()
        {
            _dapper = new dataAdapter();
        }

        public async Task<IEnumerable<StudentImport>> GetStudents()
        {
            return (await _dapper.QueryAsync<StudentImport>("func_get_students"));
        }

        internal async Task ImportStudent(StudentImport student)
        {
            var param = new
            {
                p_student_number = student.student_number,
                p_student_name = student.student_name,
                p_student_surname = student.student_surname,
                p_course_description = student.course_description,
                p_course_code = student.course_code,
                p_course_grade = student.course_grade,
            };
            await _dapper.ExecuteAsync("func_import_student", param);
        }
    }
}
