﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Domain
{
  public class Course
  {
    public int id { get; set; }
    public string course_description { get; set; }
    public string course_code { get; set; }
  }
}
