﻿namespace WebApp.Domain
{
  public class StudentImport
  {
    public StudentImport(string student_number, string student_name, string student_surname, string course_code, string course_description, string course_grade)
    {
      this.student_number = student_number;
      this.student_name = student_name;
      this.student_surname = student_surname;
      this.course_description = course_description;
      this.course_code = course_code;
      this.course_grade = course_grade;
    }
    public StudentImport()
    {

    }
    public string student_number { get; set; }
    public string student_name { get; set; }
    public string student_surname { get; set; }
    public string course_description { get; set; }
    public string course_code { get; set; }
    public string course_grade { get; set; }
  }
}
