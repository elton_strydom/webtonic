﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace WebApp.Domain
{
    public class StudentsApi
    {
        private readonly StudentsRepo _studentsRepo;

        public StudentsApi()
        {
            _studentsRepo = new StudentsRepo();
        }
        public async Task ProcessImport(IFormFileCollection files)
        {
            var importList = ProcessCsvImport(files);
            foreach (var student in importList)
                await _studentsRepo.ImportStudent(student);
        }

        internal async Task<IEnumerable<StudentImport>> GetStudents()
        {
            return await _studentsRepo.GetStudents();
        }

        public List<StudentImport> ProcessCsvImport(IFormFileCollection files)
        {
            var importList = new List<StudentImport>();
            foreach (var file in files)
            {
                using (var reader = new StreamReader(file.OpenReadStream()))
                {
                    var headerLine = reader.ReadLine();
                    while (!reader.EndOfStream)
                    {
                        var values = reader.ReadLine().Split(';');
                        // if (values.Length != _columnLength) break;
                        //values read order
                        //Student Number
                        //Firstname
                        //Surname
                        //Course Code
                        //Course Description
                        //Grade
                        importList.Add(new StudentImport(values[0], values[1], values[2], values[3], values[4], values[5]));
                    }
                }
            }
            return importList;
        }
    }
}
