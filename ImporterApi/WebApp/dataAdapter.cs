﻿using Dapper;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp
{
  public class dataAdapter
  {
    //move to app config
    private readonly string _connection = "Host=localhost;username=postgres;password=ubu.1900;Database=unidb";

    public async Task<IEnumerable<T>> QueryAsync<T>(string function, object param = null)
    {
      using (var dbConnection = new NpgsqlConnection(_connection))
      {
        dbConnection.Open();
        var result = (await dbConnection.QueryAsync<T>(function, param, commandType: CommandType.StoredProcedure));
        return result;
      }
    }

    public async Task<int> ExecuteAsync(string function, object param = null)
    {
      using (var dbConnection = new NpgsqlConnection(_connection))
      {
        dbConnection.Open();
        var result = await dbConnection.ExecuteAsync(function, param, commandType: CommandType.StoredProcedure);
        return result;
      }
    }
  }
}
