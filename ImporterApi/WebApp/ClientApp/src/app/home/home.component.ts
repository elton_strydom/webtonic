import { Component } from '@angular/core';
import { StudentRepository } from '../Repositories/student-repository';
import { ColDef } from 'ag-grid-community';
import { ViewChild } from '@angular/core';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [StudentRepository]
})

export class HomeComponent {
  studentsRepository: StudentRepository;
  successfullImport: boolean;
  rowData = [];
  columnDefs: ColDef[] = [
    { headerName: 'Student Number', field: 'student_number', sortable: true },
    { headerName: 'Name', field: 'student_name', sortable: true },
    { headerName: 'Surname', field: 'student_surname', sortable: true },
    { headerName: 'Course Code', field: 'course_code', sortable: true },
    { headerName: 'Course Description', field: 'course_description', sortable: true },
    { headerName: 'Course Grade', field: 'course_grade', sortable: true }
  ];

  public constructor(studentsRepository: StudentRepository) {
    this.studentsRepository = studentsRepository;
    this.getStudentData().then(data => {
      this.rowData = data;
    })
  }

  async uploadFile(files, event) {
    await this.studentsRepository.importFiles(files);
    //todo proper error handling
    this.successfullImport = true;
    this.rowData = await this.getStudentData();
    // Clear the input
    event.target.value = null;
  }

  async getStudentData() {
    return await this.studentsRepository.getStudents();;
  }
}
