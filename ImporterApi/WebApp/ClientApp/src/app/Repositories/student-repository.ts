import { Inject } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';

export class StudentRepository {
  http: HttpClient;
  baseUrl: string;

  public constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
    this.http = http;
  }

  async importFiles(files) {
    if (files.length === 0)
      return;

    const formData = new FormData();

    for (const file of files) {
      formData.append(file.name, file);
    }

    return await this.http.request(new HttpRequest('POST', this.baseUrl + 'import/upload', formData)).toPromise();
  }

  async getStudents() {
    return await this.http.get<StudentImport[]>(this.baseUrl + 'students').toPromise();
  }
}

interface StudentImport {
  student_number: String;
  student_name: string;
  student_surname: string;
  course_code: string;
  course_description: string;
  course_grade: string;
}