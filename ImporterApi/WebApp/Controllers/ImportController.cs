﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApp.Domain;

namespace WebApp.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class ImportController : ControllerBase
  {
    [HttpPost]
    [Route("upload")]
    public async Task<IActionResult> Upload()
    {
      var files = Request.Form.Files;
      var studentsApi = new StudentsApi();
      await studentsApi.ProcessImport(files);
      return Ok();
    }
  }
}
