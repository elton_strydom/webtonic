﻿Set-Location $PSScriptRoot
$servername = 'localhost'
$username = 'postgres'
$database = 'unidb'
$env:PGPASSWORD = 'ubu.1900'

#relative path to sql from powershell folder
$sqldb = Get-ChildItem  ..\sql\*.sql

#location of postgresql installation
Set-Location 'C:\Program Files\PostgreSQL\14\bin\';

ForEach ($f in $sqldb)
{
  $logmsg = 'SQL script {0}' -f $f.FullName
  Write-Host $logmsg
  #-w surpresses prompt for password and uses PGPASSWORD environment variable
  .\psql -h $servername -U $username -w -d $database -f $f         
}