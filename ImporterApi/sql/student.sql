/*1. view_students__________________________________________________________________*/
/*2. func_get_students______________________________________________________________*/
/*3. func_get_students______________________________________________________________*/
/*4. func_add_student_______________________________________________________________*/
/*5. func_import_student____________________________________________________________*/

/*__________________________________________________________________________________*/
/*1.view_students___________________________________________________________________*/
/*__________________________________________________________________________________*/
DROP VIEW IF EXISTS view_student CASCADE;

CREATE OR REPLACE VIEW view_student AS
SELECT s.student_number,
       s.name        AS student_name,
       s.surname     AS student_surname,
       c.description AS course_description,
       c.code        AS course_code,
       g.description AS course_grade
FROM student s
       JOIN student_grades sg ON s.id = sg.student_id
       JOIN grades g ON sg.grade_id = g.id
       JOIN course c ON sg.course_id = c.id
WHERE sg.active IS TRUE;

/*__________________________________________________________________________________*/
/*1. func_get_students______________________________________________________________*/
/*__________________________________________________________________________________*/
CREATE OR REPLACE FUNCTION func_get_students()
  RETURNS SETOF view_student
AS
$BODY$
BEGIN
  RETURN QUERY
    SELECT * FROM view_student;
END;
$BODY$
  LANGUAGE plpgsql SECURITY DEFINER
                   SET search_path = appschema, public;

/*__________________________________________________________________________________*/
/*2. func_add_student_______________________________________________________________*/
/*----------------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION func_add_student(p_name TEXT,
                                            p_surname TEXT,
                                            p_student_number TEXT)
  RETURNS INTEGER
AS
$BODY$
DECLARE
  v_student_id INTEGER;
BEGIN
  IF NOT EXISTS(SELECT 1 FROM student WHERE student_number = p_student_number)
  THEN
    INSERT INTO student (name, surname, student_number)
    VALUES (p_name, p_surname, p_student_number)
    RETURNING id INTO v_student_id;
  ELSE
    --update if an existing student found incase name and surname change was requested for some reason
    UPDATE student
    SET name    = p_name,
        surname = p_surname
    WHERE student_number = p_student_number;
    v_student_id := (SELECT id FROM student WHERE student_number = p_student_number);
  END IF;
  RETURN v_student_id;
END;
$BODY$
  LANGUAGE plpgsql SECURITY DEFINER
                   SET search_path = appschema, public;

/*__________________________________________________________________________________*/
/*3. func_import_student____________________________________________________________*/
/*----------------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION func_import_student(p_student_number TEXT,
                                               p_student_name TEXT,
                                               p_student_surname TEXT,
                                               p_course_description TEXT,
                                               p_course_code TEXT,
                                               p_course_grade TEXT)
  RETURNS VOID
AS
$BODY$
DECLARE
  v_student_id INTEGER;
  v_course_id  INTEGER;
  v_grade_id   INTEGER;
BEGIN
  v_student_id := (SELECT * FROM func_add_student(p_student_name, p_student_surname, p_student_number));
  v_course_id := (SELECT * FROM func_add_course(p_course_description, p_course_code));
  v_grade_id := (SELECT id FROM grades WHERE description = p_course_grade);

  PERFORM func_add_student_grade(v_course_id, v_grade_id, v_student_id);
END;
$BODY$
  LANGUAGE plpgsql SECURITY DEFINER
                   SET search_path = appschema, public;