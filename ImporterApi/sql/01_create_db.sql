--drop schema
DROP SCHEMA public CASCADE;
--recreate schema
CREATE SCHEMA public;
--create tables
CREATE TABLE student
(
  id             SERIAL PRIMARY KEY NOT NULL,
  student_number TEXT               NOT NULL,
  name           TEXT               NOT NULL,
  surname        TEXT               NOT NULL
);

CREATE TABLE course
(
  id          SERIAL PRIMARY KEY NOT NULL,
  description TEXT               NOT NULL,
  code        TEXT               NOT NULL
);

CREATE TABLE grades
(
  id          SERIAL PRIMARY KEY NOT NULL,
  description TEXT               NOT NULL
);
INSERT INTO grades (description)
VALUES ('A'),
       ('B'),
       ('C'),
       ('D'),
       ('E'),
       ('F');

CREATE TABLE student_grades
(
  student_id INTEGER NOT NULL,
  course_id  INTEGER NOT NULL,
  grade_id   INTEGER,
  active     BOOLEAN NOT NULL DEFAULT TRUE,
  CONSTRAINT student_id_fk FOREIGN KEY (student_id) REFERENCES student (id),
  CONSTRAINT course_id_fk FOREIGN KEY (course_id) REFERENCES course (id),
  CONSTRAINT grade_id_fk FOREIGN KEY (grade_id) REFERENCES grades (id)
);
CREATE INDEX ON student_grades (student_id);
CREATE INDEX ON student_grades (course_id);
CREATE INDEX ON student_grades (grade_id);
