/*1. func_add_student_grade_________________________________________________________*/

/*__________________________________________________________________________________*/
/*1. func_add_student_grade____________________ ____________________________________*/
/*__________________________________________________________________________________*/
CREATE OR REPLACE FUNCTION func_add_student_grade(p_course_id INTEGER, p_grade_id INTEGER, p_student_id INTEGER)
  RETURNS VOID
AS
$BODY$
BEGIN
  UPDATE student_grades
  SET active = FALSE
  WHERE student_id = p_student_id
    AND grade_id = p_grade_id;

  INSERT INTO student_grades (student_id, course_id, grade_id)
  VALUES (p_student_id, p_course_id, p_grade_id);
END;
$BODY$
  LANGUAGE plpgsql SECURITY DEFINER
                   SET search_path = appschema, public;