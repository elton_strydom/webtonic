/*1. func_add_course________________________________________________________________*/

/*__________________________________________________________________________________*/
/*1. func_add_course________________________________________________________________*/
/*__________________________________________________________________________________*/
CREATE OR REPLACE FUNCTION func_add_course(p_course_description TEXT, p_course_code TEXT)
  RETURNS INTEGER
AS
$BODY$
DECLARE
  v_course_id INTEGER;
BEGIN
  IF NOT EXISTS(SELECT 1 FROM course WHERE description = p_course_description AND code = p_course_code)
  THEN
    INSERT INTO course (description, code) VALUES (p_course_description, p_course_code) RETURNING id INTO v_course_id;
  ELSE
    v_course_id := (SELECT id FROM course WHERE description = p_course_description AND code = p_course_code);
  END IF;
  RETURN v_course_id;
END;
$BODY$
  LANGUAGE plpgsql SECURITY DEFINER
                   SET search_path = appschema, public;